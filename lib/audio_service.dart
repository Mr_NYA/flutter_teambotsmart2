import 'package:music_volume/music_volume.dart';

class AudioService {

    turnAudioMute() async {
    var _maxVolume = await MusicVolume.maxVolume;
    await MusicVolume.changeVolume(0, _maxVolume);
  }
    turnAudioOn() async {
    var _maxVolume = await MusicVolume.maxVolume;
    await MusicVolume.changeVolume(_maxVolume , _maxVolume);
  }

   turnAudioMedium() async {
    var _maxVolume = await MusicVolume.maxVolume;
    await MusicVolume.changeVolume(_maxVolume -7, _maxVolume);
  }
}