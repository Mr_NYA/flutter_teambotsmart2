import 'package:flutter/material.dart';
import './servicesChatBot.dart';


class MyButton extends StatelessWidget {
  final String name;
  final Color color;
  MyButton(this.name, this.color);
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton.icon(
        onPressed: () async {
       
          var resp = await ServicesChatBot()
              .chatBotGoogleAssistant('la meteo a oujda');
          print('la reponse du ChatBot : $resp');
        },
        icon: Icon(
          Icons.event_note,
          color: Colors.white,
        ),
        color: this.color,
        label: Text(
          name,
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            ),
        ),
      ),
    );
  }
}
