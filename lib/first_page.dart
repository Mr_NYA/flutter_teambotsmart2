import 'package:flutter/material.dart';
import './second_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Home'),
        ),
        body: Column(
          children: <Widget>[
            Center(
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ChatScreen()));
                },
                child: Text('first'),
              ),
            ),
            Container(
              child:   CircleAvatar(
                           radius: 25.0,
                           backgroundColor: Colors.amber,
                         ),
            )
          ],
        ),
      ),
    );
  }
}
