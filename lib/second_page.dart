import 'dart:async';

import 'package:flutter/material.dart';
import 'first_page.dart';
import './third_page.dart';
import 'dart:convert';
import './servicesChatBot.dart';


import './tts_service.dart';
import './enum.dart';

class SecondHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Home'),
      ),
      body: Container(
        margin: EdgeInsets.fromLTRB(100, 300, 100, 100),
        child: Column(children: <Widget>[
          Center(
            child: RaisedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ThirddHome()));
              },
              child: Text('go'),
            ),
          ),
          Center(
            child: RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('back'),
            ),
          ),
        ]),
      ),
    );
  }
}

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> with ChangeNotifier {

  // Example chats in chat screen
  List <String> chatbotmessages = [
    "la meteo a oujda",
    "quel age a tu",
    "quel est ta couleur prefere",
    "quel heure est il"
    ];
    var indice = 0;
  List<Message> messages = [
    Message(
        // isMe2: true,
        time: '5:36 PM',
        text: "Aamiin",
        isLiked: true,
        unread: true),
    Message(
        // isMe2: false,
        time: '5:30 PM',
        text: "Semoga mamah cepet sadar :')",
        isLiked: true,
        unread: false),
    Message(
        // isMe2: true,
        time: '5:36 PM',
        text: "Mungkin nanti pada saatnya mamah pulang",
        isLiked: true,
        unread: true),
    Message(
        // isMe2: false,
        time: '5:30 PM',
        text: "Mamah kemana?",
        isLiked: true,
        unread: false),
    Message(
        // isMe2: true,
        time: '5:36 PM',
        text: "Kangen sama mamah :(",
        isLiked: false,
        unread: true),
    Message(
        // isMe2: false,
        time: '7:30 PM',
        text: "Mudah-mudahan mamah cepet pulang :')",
        isLiked: false,
        unread: true),
    Message(
        // isMe2: true,
        time: '5:36 PM',
        text: "Aamiin",
        isLiked: true,
        unread: true),
    Message(
        // isMe2: false,
        time: '5:30 PM',
        text: "Semoga mamah cepet sadar :')",
        isLiked: true,
        unread: false),
    Message(
        // isMe2: true,
        time: '5:36 PM',
        text: "Mungkin nanti pada saatnya mamah pulang",
        isLiked: true,
        unread: true),
    Message(
        // isMe2: false,
        time: '5:30 PM',
        text: "Mamah kemana?",
        isLiked: true,
        unread: false),
    Message(
        // isMe2: true,
        time: '5:36 PM',
        text: "Kangen sama mamah :(",
        isLiked: false,
        unread: true),
    Message(
        // isMe2: false,
        time: '7:30 PM',
        text:
            "Mudah-mudahan mamah cepet pulang :'),Mudah-mudahan mamah cepet pulang :'),Mudah-mudahan mamah cepet pulang :')",
        isLiked: false,
        unread: true)
  ];
  var val = 0;
  var stat = false;


  _buildMessage(Message message, bool isMe) {
    final Container msg = Container(
      width: MediaQuery.of(context).size.width * 0.75,
      margin: isMe
          ? EdgeInsets.only(top: 7.0, bottom: 8.0, left: 60.0)
          : EdgeInsets.only(top: 8.0, bottom: 8.0),
      padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
      decoration: isMe
          ? BoxDecoration(
              color: Color(0x24A0ED00),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  bottomLeft: Radius.circular(15.0),
                  topRight: Radius.circular(15.0)))
          : BoxDecoration(
              color: Color(0xFFe4f1fe),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  topRight: Radius.circular(15.0),
                  bottomRight: Radius.circular(15.0))),
      child: Column(
        crossAxisAlignment:
            (isMe) ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 8.0,
          ),
          Text(message.text,
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w600)),
        ],
      ),
    );
    if (isMe) {
      return Wrap(
        children: <Widget>[
          msg,
          CircleAvatar(
            radius: 25.0,
            backgroundColor: Colors.green[200],
            child: Icon(
              Icons.person,
              color: Colors.white,
            ),
          ),
        ],
      );
    }
    return Row(
      children: <Widget>[
        CircleAvatar(
          radius: 25.0,
          backgroundColor: Colors.blue[200],
          child: Icon(
            Icons.android,
            color: Colors.white,
          ),
        ),
        msg
      ],
    );
  }



  @override
  Widget build(BuildContext context) {
    var isListening = true;
    double diametre = 10;
    // Tts_services().initTts();
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorLight,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColorLight,
        title: Text(
          'Google assistant',
          style: TextStyle(color: Colors.blue),
        ),
        centerTitle: true,
        elevation: 0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_horiz),
            iconSize: 30.0,
            color: Colors.white,
            onPressed: () {
            },
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 5.0,
            ),
            Expanded(
              child: Container(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40.0),
                        topRight: Radius.circular(40.0),
                        // bottomLeft: Radius.circular(40.0),
                        // bottomRight: Radius.circular(40.0),
                      )),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0),
                      // bottomLeft: Radius.circular(30.0),
                      // bottomRight: Radius.circular(30.0),
                    ),
                    child: ListView.builder(
                        reverse: true,
                        padding: EdgeInsets.only(top: 14.0, bottom: 20.0),
                        itemCount: messages.length,
                        itemBuilder: (BuildContext context, int index) {
                          final Message message = messages[index];
                          final bool isMe = message.unread;

                          return _buildMessage(message, isMe);
                        }),
                  )),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(
          height: 50,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    blurRadius: .26,
                    spreadRadius: diametre * 1,
                    color: isListening
                        ? Colors.blue.withOpacity(.05)
                        : Colors.white.withOpacity(.1))
              ],
              // color: finalResult ? Colors.green :((speech.isListening || isListening)? Colors.red: Colors.white),
              borderRadius: BorderRadius.all(Radius.circular(50)),
            ),
            child: Icon(Icons.mic, color: Colors.blue[200])),
        onPressed: () async {
       
         chat(chatbotmessages[indice]);
        },
        backgroundColor: Colors.white,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

Future<void> chat(msg) async {
  indice ++;
  print(indice);
   var resp;
        //  startListening();
        
      
           resp = await ServicesChatBot()
              .chatBotGoogleAssistant(msg);
             
          print('la reponse du ChatBot : $resp');
          setState(() {
            messages.insert(
                0,
                Message(
                    time: '5:36 PM',
                    text: resp,
                    isLiked: false,
                    unread: false));
                    
          });
 await Tts_services().Speak(resp);

   Tts_services().flutterTts.setCompletionHandler(() {
    
        print("Complete function");
       var stat = TtsState.complet;
        print(stat);
   chat(chatbotmessages[indice % (chatbotmessages.length)]);
    });
 


               
          
}









}

class Message {
  final bool isMe2;
  final String time;
  final String text;
  final bool isLiked;
  final bool unread;

  Message({this.isMe2, this.time, this.text, this.isLiked, this.unread});
}
