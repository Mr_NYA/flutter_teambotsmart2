import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:flutter_dialogflow/flutter_dialogflow.dart';


class ServicesChatBot {
  static const URL_GoogleAssistant =
      'http://149.202.194.24:3003/assistant/fr-FR/';
  static const URL_PondoraBot =
      'https://www.pandorabots.com/pandora/talk-xml?botid=dbf74f507e3458e4&input=';
  static const URL_Teava = 'https://teamnet-rd.ml/voicebot-api';
  static const Token_Teava = 'cLt9yYygYtVY6rCuQt1F';
  static const Token_DialogFlow = '7a7edfee5c4c4550a609d0580fbb08a3';

  //******************** */ #Region Google assistant ******************//
  chatBotGoogleAssistant(String message) async {
    var reponseChatBot = 'Désolé, je n\'ai pas de réponse';
    var url = URL_GoogleAssistant + message;
    var responseHttp = await http.get(url);
    if (responseHttp.statusCode == 201) {
      var jsonResponseHttp = convert.jsonDecode(responseHttp.body);
      if (jsonResponseHttp['output'] != null) {
        reponseChatBot = jsonResponseHttp['output'];
      }

    } else {
      print('erreur de requette avec status: ${responseHttp.statusCode}.');
    }
    return reponseChatBot;
  }

//********************* #EndRegion ******//

//********************* #region Pandorabot*************************//

  chatBotPandoraBot(String message) async {
    var reponseChatBot = '';
    var url = URL_PondoraBot + message + '&format=json';
    var responseHttp = await http.get(url);

    if (responseHttp.statusCode == 200) {
      var jsonResponseHttp = convert.jsonDecode(responseHttp.body);
      if (jsonResponseHttp['that'] != null) {
        reponseChatBot = jsonResponseHttp['that'];
      }
    } else {
      print('erreur de requette avec status: ${responseHttp.statusCode}.');
    }
    return reponseChatBot;
  }

//////////////////#EndRegion///////////////////////////////////////



/////////////////////#Region Taeva/////////////////////

  chatBotTeava(String message) async {
    var responceChatBot = '';

    Map data = {"format": "json", "token": Token_Teava, "message": message};
    //encode Map to JSON
    var body = json.encode(data);

    var response = await http.post(URL_Teava,
        headers: {"Content-Type": "application/json"}, body: body);
    print("${response.statusCode}");
    // print("${response.body}");
    responceChatBot = convert.jsonDecode(response.body)['that'];
    // print('la reponse est ==> $responceChatBot');
    return responceChatBot;
  }

////////////////////////// #EndRegion/////////////////////

/////////////////////#Region  DialogFlow /////////////////

  chatBotDialogFlow(String message) async {

var reponseChatBot = 'Désolé, je n\'ai pas de réponse';
    Dialogflow dialogflow = Dialogflow(token: Token_DialogFlow);
    try{
    AIResponse response = await dialogflow.sendQuery(message);
    reponseChatBot = response.getMessageResponse();
    }
    catch (e){
      print('Erreur DialogFlow $e');
    }
    
    return reponseChatBot;
  }

///////////////////// #EndRegion//////////////////////
}
