import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:navigation_demo/tts_service.dart';
import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

import 'package:flutter/services.dart';
import './servicesChatBot.dart';

class Stt_service {
  final SpeechToText speech = SpeechToText();

// variable a utililser
  String lastWords = ""; // la phrase ecouté
  String lastStatus = ""; //affecter le status d'ecoute
  String lastError = ""; // l'erreur retenu au cas d'erreur

  List<LocaleName> _localeNames = []; // tableau des langues supporté
  String _currentLocaleId =
      ""; // la langue selectionnée par defaut celle du systeme

  bool _hasSpeech = false; // pour confirmer le support du stt avec le systeme
  bool isListening = false; // indicateur de l'ecoute
  bool finalResult = false; // indicateur true si un texte est reconnu
  bool modePermanantFlag = true; //false pour desactiver le mode permanant
  bool flagInficeResult = true; // pour eviter d'avoir deux resultat
  bool flagStartListening = false;

  double level = 0.0;
  int conteurNbTentativeEcoute = 0;

  Future<void> initSpeechState() async {
    bool hasSpeech = await speech.initialize(
        onError: errorListener, onStatus: statusListener);
    if (hasSpeech) {
      _localeNames = await speech.locales();

      var systemLocale = await speech.systemLocale();
      _currentLocaleId = systemLocale.localeId;
    }

    _hasSpeech = hasSpeech;
  }

  void startListening() {
    lastWords = "";
    lastError = "";
    speech.listen(
        onResult: resultListener,
        // listenFor: Duration(seconds: 10),
        localeId: _currentLocaleId,
        onSoundLevelChange: soundLevelListener,
        cancelOnError: true,
        partialResults: true
        );
    isListening = true;
    print('start listening $isListening');
  }

  void stopListening() {
    speech.stop();

    level = 0.0;
    isListening = false;
    print('stop listening $isListening');
  }

  void cancelListening() {
    speech.cancel();

    level = 0.0;
  }

  void soundLevelListener(double level) {
    this.level = level;
  }

  resultListener(SpeechRecognitionResult result) async {
    lastWords = '';
    lastWords = result.recognizedWords;
    finalResult = result.finalResult;
    
    print('final result $finalResult');
    if (finalResult && flagInficeResult) {
      flagInficeResult = false; // pour eviter 2 resultats
      print('le texte reconnu $lastWords');
      flagStartListening = true; // pour empechr de lancer la prochain reconnaissance
      var resp = await ServicesChatBot().chatBotGoogleAssistant(lastWords);
      print(resp);
       Tts_services().Speak(resp);
      conteurNbTentativeEcoute = 0; // pour relancer le conteur
       Tts_services().flutterTts.setCompletionHandler(() {
         print('TTS terminé');
       
    
        
          startListening();
          flagInficeResult =
              true; // pour reactiver la recuperation de resultat
          flagStartListening = false;
        
      
    
      
    });
       
    }
      else
        {
          print('je vais sortir du mode ecoute permanant');
          // flagStartListening = true;
        }
  }

  void statusListener(String status) {
    lastStatus = "$status";
    print('le status du STT $lastStatus');
    print('flagStartListening dans statuslistener ==> $flagStartListening');
   Timer(Duration(milliseconds: 1000), () { // pour eviter le deuxieme lancement avant la reconnaissance du premier 
    print('flagStartListening dans statuslistener 2 ==> $flagStartListening');
    if (lastStatus != 'listening' && modePermanantFlag && !flagStartListening) {
      print('flagStartListening dans statuslistener 3 ==> $flagStartListening');
        if (conteurNbTentativeEcoute < 3) {
          print('avant lancemet du prochain listening indicateur conteur $conteurNbTentativeEcoute');
          startListening();
          flagInficeResult =
              true; // pour reactiver la recuperation de resultat
          conteurNbTentativeEcoute++;
        }
     
    }
     });
  }

  void errorListener(SpeechRecognitionError error) {
    print(
        "Erreur sur l'initialisation du STT ${error.errorMsg} - ${error.permanent}");
  }


}
