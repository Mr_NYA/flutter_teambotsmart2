import 'package:flutter/material.dart';
import 'first_page.dart';
import './stt_service.dart';
import 'audio_service.dart';

class ThirddHome extends StatefulWidget {

  @override
  _ThirddHomeState createState() => _ThirddHomeState();
   
}


class _ThirddHomeState extends State<ThirddHome> {

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('third Home'),
      ),
      body: new Center(
          child: Wrap(children: <Widget>[
        RaisedButton(
          onPressed: () {
            Stt_service().initSpeechState();
          },
          child: Text('initSpeechState'),
        ),
         RaisedButton(
          onPressed: () {
            setState(() {
               Stt_service().startListening();
            });
           
          },
          child: Text('start listening'),
        ),
         RaisedButton(
          onPressed: () {
            setState(() {
               Stt_service().stopListening();
            });
           
          },
          child: Text('stop listening'),
        ),
         RaisedButton(
          onPressed: () {
            setState(() {
               Stt_service().cancelListening();
            });
           
          },
          child: Text('cancel listening'),
        ),
           RaisedButton(
          onPressed: () {
            AudioService().turnAudioMute();
          },
          child: Text('turn audio off'),
        ),
         RaisedButton(
          onPressed: () {
            AudioService().turnAudioMedium();
          },
          child: Text('turn audio medium'),
        ),
          RaisedButton(
          onPressed: () {
            AudioService().turnAudioOn();
          },
          child: Text('turn audio On'),
        ),
      ]
      )
      ),
    );
  }

}
