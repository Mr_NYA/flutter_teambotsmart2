import 'dart:async';

import 'package:flutter_tts/flutter_tts.dart';
import './enum.dart';

class Tts_services {
  
   FlutterTts flutterTts = FlutterTts();
   double volume = 0.5;
  double pitch = 1.0;
  double rate = 0.5;
  var stat;

   initTts() {
   


    flutterTts.setStartHandler(() {
     
        print("playing");
      stat =  TtsState.playing;
      print(stat);
      
    });

    flutterTts.setCompletionHandler(() {
    
        print("Complete");
        stat = TtsState.complet;
        print(stat);
    
    });

    flutterTts.setErrorHandler((msg) {
    
        print("error: $msg");
     
    });
  }

     Speak(texte) async {
    await flutterTts.setVolume(volume);
    await flutterTts.setSpeechRate(rate);
    await flutterTts.setPitch(pitch);

    if (texte != null) {
      if (texte.isNotEmpty) {
        await flutterTts.speak(texte);
      
      }
    }
//     Timer(Duration(seconds: 3), (){
// print('hi there $stat');
//     });
    
  }

  getStat(){
    
    flutterTts.setCompletionHandler(() {
    
        print("Complete function");
        stat = TtsState.complet;
        print(stat);
        if(stat != null)
    return stat;
    });
    
  }
  
}

